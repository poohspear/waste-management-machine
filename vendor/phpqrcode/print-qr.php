<?php

error_reporting(E_ALL);
ini_set('display_errors', 1);

$param = $_GET['q'];

include "qrlib.php";

ob_start(); 
 
// here DB request or some processing 
$codeText = $param; 
 
// end of processing here 
$debugLog = ob_get_contents(); 
ob_end_clean(); 
 
// outputs image directly into browser, as PNG stream 
QRcode::png($codeText, false , QR_ECLEVEL_L, 8);

?>