<?php

error_reporting(E_ALL);
ini_set('display_errors', 1);

$timestamp = time();
$token = uniqid();
$location = "HLD-1";
$weight = rand(10,100);

// [location]comma[token]comma[timestamp]comma[weight]kg


$increased_text = "{$location}comma{$token}comma{$timestamp}comma{$weight}kg";
$encoded = base64_encode(utf8_encode($increased_text));

echo "
    <img 
    style=\"max-width: 200px;margin-bottom: 20px;margin-top: 20px;\"
        src=\"./vendor/phpqrcode/print-qr.php?q=" . $encoded . "\" 
    class=\"img-responsive\" />
    <h1 class=\"mb-3\" style='font-size:18px;'>Congratulations</h1>
    <p class=\"mb-3\" style='font-size:18px;'>You got " . $increased_text . " </p>
    <p class=\"mb-3\" style='font-size:18px;'>You got " . $encoded . " </p>
    <p class=\"mb-5\" style='font-size:18px;'>
        Please scan the QR code to complete your process. 
    </p>
";


?>