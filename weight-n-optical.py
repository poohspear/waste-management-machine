#! /usr/bin/python2

import sys
import time
import RPi.GPIO as transput
from hx711 import HX711
dataP=18
transput.setwarnings(False)
transput.setmode(transput.BCM)    
transput.setup(dataP,transput.IN)
load = HX711(5, 6)  #DT,SCK
load.set_reading_format("MSB", "MSB")
load.set_reference_unit(-102)  #-102
load.reset()
load.tare()

while True:
    try:
        text = open("/var/www/html/waste-management-machine/weight-n-optical.txt","w")
        gram = load.get_weight(5)
        data = int(not transput.input(dataP))
        if gram < 80:
            gram = 0
        else:
            gram = gram
            
        kilogram = gram/1000.0
        print (str(data)+","+str(kilogram))
        text.write(str(data)+","+str(kilogram))
        
        load.power_down()
        load.power_up()
       # time.sleep(0.1)
        
    except (KeyboardInterrupt, SystemExit):
        text.close()
        transput.cleanup()
        sys.exit()
