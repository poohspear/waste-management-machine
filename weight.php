<?php

ob_start();
session_start();


require("config.php");


$file_txt = file_get_contents('weight-n-optical.txt');
$file_txt = explode(',', $file_txt);
$file_txt = $file_txt[1];


if(!isset($_SESSION['prev_file_text'])){
    $_SESSION['prev_file_text'] = 0;
}


if(!isset($_SESSION['last_update'])){
    $_SESSION['last_update'] = strtotime(date('Y/m/d H:i:s'));
}


if(!isset($_SESSION['last_update_file_text'])){
    $_SESSION['last_update_file_text'] = "0";
}


if(!isset($_SESSION['prev_check'])){
    $_SESSION['prev_check'] = strtotime(date('Y/m/d H:i:s'));
}


// Get last update
$now_update = strtotime(date('Y/m/d H:i:s'));


// Get last update
if(
    $file_txt != $_SESSION['last_update_file_text'] &&
    $file_txt > ( abs($_SESSION['last_update_file_text']) + .090 )
){
    $_SESSION['last_update_file_text'] = $file_txt;
    $_SESSION['last_update'] = strtotime(date('Y/m/d H:i:s'));
}else{
    $_SESSION['last_update'] = $_SESSION['last_update'];
}

// Get different seconds
// $diff = $_SESSION['last_update'] - $_SESSION['prev_check'];
$diff = $now_update - $_SESSION['last_update'];

echo "<!-- Last Update Time ".$_SESSION['last_update']." --> \n";
echo "<!-- Previous Check Time ".$_SESSION['prev_check']." --> \n";
echo "<!-- Different time: ".$diff." --> \n";
echo "<!-- Wait Second: ".(round($weight_wait_seconds,2)+60)." --> \n";

echo "<!-- File Txt: ".$file_txt." --> \n";
echo "<!-- Prev File Txt: ".$_SESSION['prev_file_text']." --> \n";
echo "<!-- Last Update File Text: ".$_SESSION['last_update_file_text']." --> \n";




if(
    $diff > $weight_wait_seconds && 
    $diff < (round($weight_wait_seconds,2)+60)
){


    $check_firt_time = $_SESSION['last_update'];


    if(!isset($_COOKIE[$check_firt_time])){
        echo "<!-- in the loop -->\n";

        setcookie($check_firt_time, "i have value", time()+3600);  /* expire in 1 hour */

        echo "<!-- Check first time: ". $_COOKIE[$check_firt_time] ." --> \n";

        $_SESSION['last_update_timestamp'] = time();
        $_SESSION['last_update_weight'] = round($file_txt,2)-round($_SESSION['prev_file_text'],2);
        $_SESSION['last_update_weight'] = round($_SESSION['last_update_weight'],2);
        $_SESSION['last_update_token'] = uniqid();
    }else{
        echo "<!-- outside of the loop -->";
    }


    $_SESSION['prev_file_text'] = $file_txt;
    $_SESSION['last_update_file_text'] = $file_txt;
    $_SESSION['prev_check'] = $_SESSION['last_update'];
    $timeleft = round($weight_wait_seconds+60,2) - $diff;



    $timestamp = $_SESSION['last_update_timestamp'];
    $weight = $_SESSION['last_update_weight'];
    $token = $_SESSION['last_update_token'];



    echo "<!-- Timestamp: ".$timestamp." --> \n";
    echo "<!-- Weight: ".$weight." --> \n";
    echo "<!-- Token: ".$token." --> \n";


    // [location]comma[token]comma[timestamp]comma[weight]kg
    $data_string = "{$location}comma{$token}comma{$timestamp}comma{$weight}kg";
    $encoded = base64_encode(utf8_encode($data_string));


    echo '
        <img 
        style="max-width: 150px;margin-bottom: 20px;"
        src="./vendor/phpqrcode/print-qr.php?q='.$encoded.'" 
        class="img-responsive" />
        <h1 class="mb-3">Congratulations</h1>
        <h1 class="mb-3">You got '.$weight.' Kg</h1>
        <p>'.$timeleft.' Sec</p>
        <p class="mb-5">
            Please scan the QR code to complete your process. 
        </p>
    ';
}elseif (
            $diff > ( 
                        round($weight_wait_seconds,2) + 60 
                    )
        ) {
    echo "<h1>Thank you!</h1>";
    echo "<p>Thank for choosing us!</p>";
    echo "<h1><br /><br /></h1>";
}else{
    $timeleft = 6 - $diff;
    echo "<h1>Processing ...</h1>";
    echo "<p>Please wait {$timeleft} sec.</p>";
    echo "<h1><br /><br /></h1>";
}


?>