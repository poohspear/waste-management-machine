<?php


include("config.php");


// Sensor Update
if(
    isset($_POST['weight_wait_seconds']) && 
    isset($_POST['sensor_wait_seconds']) && 
    isset($_POST['sensor_distance'])
){
    $weight_wait_seconds = $_POST['weight_wait_seconds'];
    $sensor_wait_seconds = $_POST['sensor_wait_seconds'];
    $sensor_distance = $_POST['sensor_distance'];

    // Update Config File
    $myfile = fopen("config.php", "w") or die("Unable to open file!");
    $txt = "<?php\n
    \$weight_wait_seconds = ".$weight_wait_seconds.";\n
    \$sensor_wait_seconds = ".$sensor_wait_seconds.";\n
    \$sensor_distance = ".$sensor_distance.";\n
    \$location = \"".$location."\";\n
    ?>";
    fwrite($myfile, $txt);
    fclose($myfile);

}


// Location Update
if( isset($_POST['location']) ){
    $location = $_POST['location'];

    // Update Config File
    $myfile = fopen("config.php", "w") or die("Unable to open file!");
    $txt = "<?php\n
    \$weight_wait_seconds = ".$weight_wait_seconds.";\n
    \$sensor_wait_seconds = ".$sensor_wait_seconds.";\n
    \$sensor_distance = ".$sensor_distance.";\n
    \$location = \"".$location."\";\n
    ?>";
    fwrite($myfile, $txt);
    fclose($myfile);

}


// Total Weight in Trash Config
if( isset($_POST['total_weight']) ){
    $total_weight = $_POST['total_weight'];


    // Update Config File
    $myfile = fopen("weight.txt", "w") or die("Unable to open file!");
    $txt = $total_weight;
    fwrite($myfile, $txt);
    fclose($myfile);

}
$total_weight = file_get_contents("weight.txt");


// Distance From Sensor Config
if( isset($_POST['distance_from_sensor']) ){
    $distance_from_sensor = $_POST['distance_from_sensor'];


    // Update Config File
    $myfile = fopen("sensor.txt", "w") or die("Unable to open file!");
    $txt = $distance_from_sensor;
    fwrite($myfile, $txt);
    fclose($myfile);

}
$distance_from_sensor = file_get_contents("sensor.txt");


?><!DOCTYPE html>
<html>
<head>
    <title>Weight Sensor</title>
    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
</head>
<body>


    <div class="container">
        <div class="col-8 offset-2">




            <h1>&nbsp;</h1>
            <p class="text-right">
                <a href="wmm-config.php" class="btn btn-outline-primary">Reload</a>
            </p>
            <h1>&nbsp;</h1>



            <!-- Location Config -->
            <h1 class="text-center" id="location">Location Config</h1>
            <form action="wmm-config.php" method="post">
                <div class="form-group">
                    <label for="location">Location</label>
                    <input 
                        type="text" 
                        name="location" 
                        class="form-control" 
                        value="<?=$location?>">
                </div>
                <div class="form-group text-right">
                      <button type="reset" class="btn btn-outline-primary">Reset</button>
                      <button type="submit" class="btn btn-primary">Save Change</button>
                </div>
            </form>




            <!-- Sensor Config -->
            <h1 class="text-center" id="weight_wait_seconds">Sensor Config</h1>
            <form action="wmm-config.php" method="post">
                <div class="form-group">
                    <label for="weight_wait_seconds">Weight Wait Seconds</label>
                    <input 
                        type="text" 
                        name="weight_wait_seconds" 
                        class="form-control" 
                        value="<?=$weight_wait_seconds?>">
                </div>
                <div class="form-group">
                    <label for="sensor_wait_seconds">Sensor Wait Seconds</label>
                    <input 
                        type="text" 
                        name="sensor_wait_seconds" 
                        id="input" 
                        class="form-control" 
                        value="<?=$sensor_wait_seconds?>">
                </div>
                <div class="form-group">
                    <label for="sensor_distance">Sensor Distance</label>
                    <input 
                        type="text" 
                        name="sensor_distance" 
                        class="form-control" 
                        value="<?=$sensor_distance?>">
                </div>
                <div class="form-group text-right">
                      <button type="reset" class="btn btn-outline-primary">Reset</button>
                      <button type="submit" class="btn btn-primary">Save Change</button>
                </div>
            </form>




            <!-- Total Weight in Trash Config -->
            <h1 class="text-center" id="total_weight">Total Weight in Trash</h1>
            <form action="wmm-config.php" method="post">
                <div class="form-group">
                    <label for="total_weight">Total Weight</label>
                    <input 
                        type="text" 
                        name="total_weight" 
                        class="form-control" 
                        value="<?=$total_weight?>">
                </div>
                <div class="form-group text-right">
                      <button type="reset" class="btn btn-outline-primary">Reset</button>
                      <button type="submit" class="btn btn-primary">Save Change</button>
                </div>
            </form>




            <!-- Distance From Sensor Config -->
            <h1 class="text-center" id="distance_from_sensor">Distace From Sensor</h1>
            <form action="wmm-config.php" method="post">
                <div class="form-group">
                    <label for="distance_from_sensor">Distance from Sensor</label>
                    <input 
                        type="text" 
                        name="distance_from_sensor" 
                        class="form-control" 
                        value="<?=$distance_from_sensor?>">
                </div>
                <div class="form-group text-right">
                      <button type="reset" class="btn btn-outline-primary">Reset</button>
                      <button type="submit" class="btn btn-primary">Save Change</button>
                </div>
            </form>




        </div>
    </div>




    <!-- Bootstrap core JavaScript -->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>




</body>
</html>