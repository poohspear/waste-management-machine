#! /usr/bin/python2

import sys
import time
import RPi.GPIO as transput

pDis = 0
trig = 18
echo = 24

transput.setwarnings(False)    
transput.setmode(transput.BCM)
transput.setup(trig,transput.OUT)
transput.setup(echo,transput.IN)
a=1
while a == 1:
    try:
       #usn = open("/home/pi/Desktop/ultrasonic.txt","w")
        
        transput.output(trig,True)
        time.sleep(0.00001)
        transput.output(trig,False)

        beginT = time.time()
        endT = time.time()

        while transput.input(echo) == 0:
            beginT = time.time()

        while transput.input(echo) == 1:
            endT = time.time()

        diffT = (endT - beginT)

        distance = diffT * 17150
        
        if distance > 200.0:
            distance = pDis
        else:
            distance = distance

            
        print distance
       #usn.write(str(distance))
        #time.sleep(0.1)
        pDis = distance
        a = 0
      
    except (KeyboardInterrupt, SystemExit):
        #usn.close()
        transput.cleanup()
        sys.exit()

  
    
