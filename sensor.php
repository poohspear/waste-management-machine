<?php
session_start();
require("config.php");

ini_set('display_errors', 1);

// $command = escapeshellcmd('sudo python ultrasonic.py') or die("Error 1");
// $file_txt = shell_exec($command) or die("Error 2");
// $file_txt = floatval($file_txt);

$file_txt = "5";

// Get pre check
if(!isset($_SESSION['sensor_prev_check'])){
	$_SESSION['sensor_prev_check'] = strtotime(date('Y/m/d H:i:s'));
}


// Get last update
if(
		$file_txt < $sensor_distance || 
		!isset($_SESSION['sensor_last_update'])
){
	$_SESSION['sensor_last_update'] = strtotime(date('Y/m/d H:i:s'));
}else{
	$_SESSION['sensor_last_update'] = strtotime(date('Y/m/d H:i:s'));
}

// Get different seconds
$diff = $_SESSION['sensor_prev_check'] - $_SESSION['sensor_last_update'];

if($diff < $sensor_wait_seconds && $file_txt < $sensor_distance ){
	$prev_file_text = $file_txt;
	$increased_text = $prev_file_text - $file_txt;
	$_SESSION['sensor_prev_check'] = $_SESSION['sensor_last_update'];
	echo 'true';
}else{
	echo 'false';
}

?>